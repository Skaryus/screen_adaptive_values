library screen_adaptive_values;

import 'dart:ui';

import 'package:flutter/widgets.dart';

class ScreenAdaptiveValues {
  static late double _minMediumHeight;
  static late double _maxMediumHeight;
  static late double _minMediumWidth;
  static late double _maxMediumWidth;

  static void init(
      {double? minMediumHeight,
      double? maxMediumHeight,
      double? minMediumWidth,
      double? maxMediumWidth}) {
    _minMediumHeight = minMediumHeight ?? 480;
    _maxMediumHeight = maxMediumHeight ?? 900;
    _minMediumWidth = minMediumWidth ?? 600;
    _maxMediumWidth = maxMediumWidth ?? 840;
  }

  static bool _isHeightSmall(Size size) => size.height < _minMediumHeight;
  static bool _isHeightMedium(Size size) =>
      size.height < _maxMediumHeight && size.height > _minMediumHeight;
  static bool _isHeightLarge(Size size) => size.height > _maxMediumHeight;

  static T heightAdaptiveValue<T>(BuildContext context,
      {required T small, required T medium, required T large}) {
    final size = MediaQuery.sizeOf(context);

    if (_isHeightSmall(size)) {
      return small;
    } else if (_isHeightMedium(size)) {
      return medium;
    } else if (_isHeightLarge(size)) {
      return large;
    } else {
      throw Exception();
    }
  }

  static ScreenSizeType heightScreenSizeType(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    if (_isHeightSmall(size)) {
      return ScreenSizeType.small;
    } else if (_isHeightMedium(size)) {
      return ScreenSizeType.medium;
    } else if (_isHeightLarge(size)) {
      return ScreenSizeType.large;
    } else {
      throw Exception();
    }
  }

  static bool _isWidthSmall(Size size) => size.width < _minMediumWidth;
  static bool _isWidthMedium(Size size) =>
      size.width <= _maxMediumWidth && size.width >= _minMediumWidth;
  static bool _isWidthLarge(Size size) => size.width > _maxMediumWidth;

  static T widthAdaptiveValue<T>(BuildContext context,
      {required T small, required T medium, required T large}) {
    final size = MediaQuery.sizeOf(context);

    if (_isWidthSmall(size)) {
      return small;
    } else if (_isWidthMedium(size)) {
      return medium;
    } else if (_isWidthLarge(size)) {
      return large;
    } else {
      throw Exception();
    }
  }

  static ScreenSizeType widthScreenSizeType(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    if (_isWidthSmall(size)) {
      return ScreenSizeType.small;
    } else if (_isWidthMedium(size)) {
      return ScreenSizeType.medium;
    } else if (_isWidthLarge(size)) {
      return ScreenSizeType.large;
    } else {
      throw Exception();
    }
  }

  static getDeviceSize(BuildContext context) {
    final view = View.of(context);
    return Size(view.physicalSize.width / view.devicePixelRatio,
        view.physicalSize.height / view.devicePixelRatio);
  }
}

enum ScreenSizeType { small, medium, large }
